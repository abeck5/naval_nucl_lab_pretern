# Author: Alex Beck
# E-mail: abeck5@nd.edu
#
# This is the Makefile for the Naval Nuclear
# Laboratory Preternship for Fall 2019 CSE 20133

# GCC is for the GCC compiler for C
CC := gcc

# G++ is for the GCC compiler for C++
PP := g++

# CFLAGS are the compiler flages for when we compile C code in this course 
FLAGS := -O0 -g -Wall -Wextra -Wconversion -Wshadow -pedantic -Werror 
CFLAGS := -std=c11 $(FLAGS)
CXXFLAGS := -m64 -std=c++11 -Weffc++ $(FLAGS) 

# Variable for folder
folder := ~/naval_nucl_lab_pretern

# Command: make main
mainObj :=  $(folder)/main.o $(folder)/reactor.o

main: $(mainObj)
	$(PP) $(CXXFLAGS) -o $(folder)/main $(mainObj)

main.o: $(folder)/main.cpp $(folder)/reactor.h
	$(PP) $(CXXFLAGS) -c $(folder)/main.cpp
	
reactor.o: $(folder)/reactor.cpp $(folder)/reactor.h
	$(PP) $(CXXFLAGS) -c $(folder)/reactor.cpp



# make all
all: main

# make clean
clean:
	rm -rf *.o main
