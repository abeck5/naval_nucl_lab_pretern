# Naval_nucl_lab_pretern

This is the preternship project for CSE 20133 for the Naval Nuclear Laboratory. This project will serve as a prototype warning system that simulates the changing coolant temperatures and pressures in a nuclear reactor.

In order to run this program run the commands:

make

./main

reactordata.txt (This is the file made for this project. If one wishes to use a different data file, type in the name of the file here and press enter.)

make clean (To remove the object files and executables)
