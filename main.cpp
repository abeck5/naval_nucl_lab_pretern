/**********************************************
* File: main.cpp
* Author: Alex Beck
* Email: abeck5@nd.edu
* 
* This is the main file for the program 
**********************************************/


#include "reactor.h"

void readIn(std::vector<Reactor> & text);

/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
* 
* This is the main driver function for the 
* program 
********************************************/
int main(){
	
	std::vector<Reactor> reactorVec;
	readIn(reactorVec);
	for (auto i = reactorVec.begin(); i != reactorVec.end(); i++){ // loop that prints out all data
		i->printData();
		i->checkTempAndPres();
	}

}


/********************************************
* Function Name  : readIn
* Pre-conditions : std::vector<Reactor> & text
* Post-conditions: none
* 
* This function reads in data from a file and
* creates objects for each input line. 
********************************************/
void readIn(std::vector<Reactor> & text){ //Loads in data to the vector from the filebuf
	std::string file;
	std::cout << "Enter a file name: ";
	std::getline(std::cin,file); //Gets user input for file name
	std::cout << std::endl;
	std::ifstream infile;
	infile.open(file);
	
	while(!infile){ //If the file doesnt exist, prompts user for more input
		std::cout << "Enter a valid file name: ";
		std::getline(std::cin,file);
		std::cout << std::endl;
		infile.open(file);
	}
	
	Reactor st; // variable for a single reactor
	std::string sTemp, sPress, sTempMax, sTempMin, sPresMax, sPresMin, sSpeed; //variables to read in the the two items from input line
	float temp, pres, tempMax, tempMin, presMax, presMin;
	int speed;
	char c = (char)infile.peek();
	while(c != EOF) {
		// Takes in data from file
		std::getline(infile, sTemp, ',');
		std::getline(infile, sPress, ',');
		std::getline(infile, sTempMax, ',');
		std::getline(infile, sTempMin, ',');
		std::getline(infile, sPresMax, ',');
		std::getline(infile, sPresMin, ',');
		std::getline(infile, sSpeed, '\n');
		
		// convert to floats
		temp = std::stof(sTemp);
		pres = std::stof(sPress);
		tempMax = std::stof(sTempMax);
		tempMin = std::stof(sTempMin);
		presMax = std::stof(sPresMax);
		presMin = std::stof(sPresMin);
		speed = std::stoi(sSpeed);
		
		st = Reactor(temp, pres, tempMax, tempMin, presMax, presMin, speed); 
		text.push_back(st); // add to the data struct
		
		c = (char)infile.peek(); //checks to see if the file will end or if there are more lines
	}
	infile.close();
	
		
} 







