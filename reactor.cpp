/**********************************************
* File: reactor.cpp
* Author: Alex Beck
* Email: abeck5@nd.edu
* 
* This is the cpp file for the Reactor class 
**********************************************/

#include "reactor.h"

float Reactor::getTemp() const { return temp; }

float Reactor::getPres() const { return pres; }

float Reactor::getMaxTemp() const { return maxTemp; }

float Reactor::getMinTemp() const { return minTemp; }

float Reactor::getMaxPres() const { return maxPres; }

float Reactor::getMinPres() const { return minPres; }

int Reactor::getPumpSpeed() const { return pumpspeed; }

void Reactor::setTemp(float newTemp) { temp = newTemp; }

void Reactor::setPres(float newPres) { pres = newPres; }

void Reactor::setPumpSpeed(int inSpeed) { pumpspeed = inSpeed; }

void Reactor::printData() const {

	std::cout << "###################################" << std::endl;
	std::cout << "  Reactor Temperature:     " << temp << std::endl;
	std::cout << "  Reactor Pressure:        " << pres << std::endl;
	std::cout << "  Reactor Max Temperature: " << maxTemp << std::endl;
	std::cout << "  Reactor Min Temperature: " << minTemp << std::endl;
	std::cout << "  Reactor Max Pressure:    " << maxPres << std::endl;
	std::cout << "  Reactor Min Pressure:    " << minPres << std::endl;
	std::cout << "  Reactor Pump Speed:      " << pumpspeed << std::endl;
	std::cout << "###################################" << std::endl << std::endl;
}

void Reactor::checkTempAndPres(){
	// Checking to see if the temp is too low
	if(getTemp() <= getMinTemp()){
		std::cout << "WARNING: Coolant temperature too low" << std::endl;
		std::cout << "Increasing pump speed from " << getPumpSpeed() << " to ";
		setPumpSpeed(getPumpSpeed() + 3);
		std::cout << getPumpSpeed() << std::endl;
		setTemp(float((((getMaxTemp() + getMinTemp()) / 2))));
		// Changing pump speed affects pressure as well
		// so this change in pump speed will set the pressure 
		// to the mid-range value
		setPres(float((getMaxPres() + getMinPres()) / 2)); 
		std::cout << "Temperature is now at " << getTemp() << "\n";
		std::cout << "Pressure is now at    " << getPres() << "\n\n";
	} 
	
	else if (getTemp() >= getMaxTemp()){ // COndition where temp is too high
		std::cout << "WARNING: Coolant temperature too high" << std::endl;
		std::cout << "Decreasing pump speed from " << getPumpSpeed() << " to ";
		setPumpSpeed(getPumpSpeed() - 3);
		std::cout << getPumpSpeed() << std::endl;
		setTemp(float(((getMaxTemp() + getMinTemp()) / 2)));
		// Changing pump speed affects pressure as well
		// so this change in pump speed will set the pressure 
		// to the mid-range value
		setPres(float((getMaxPres() + getMinPres()) / 2));
		std::cout << "Temperature is now at " << getTemp() << "\n";
		std::cout << "Pressure is now at    " << getPres() << "\n\n";
		
	}
	
	else if(getPres() <= getMinPres()){
		std::cout << "WARNING: Coolant pressure too low" << std::endl;
		std::cout << "Decreasing pump speed from " << getPumpSpeed() << " to ";
		setPumpSpeed(getPumpSpeed() - 3);
		std::cout << getPumpSpeed() << std::endl;
		setPres(float((getMaxPres() + getMinPres()) / 2));
		// Changing pump speed affects temperature as well
		// so this change in pump speed will set the temperature 
		// to the mid-range value
		setTemp(float((getMaxTemp() + getMinTemp()) / 2));
		std::cout << "Temperature is now at " << getTemp() << "\n";
		std::cout << "Pressure is now at    " << getPres() << "\n\n";
	}
	
	else if(getPres() >= getMaxPres()){
		std::cout << "WARNING: Coolant pressure too high" << std::endl;
		std::cout << "Increasing pump speed from " << getPumpSpeed() << " to ";
		setPumpSpeed(getPumpSpeed() + 3);
		std::cout << getPumpSpeed() << std::endl;
		setPres(float((getMaxPres() + getMinPres()) / 2));
		// Changing pump speed affects temperature as well
		// so this change in pump speed will set the temperature 
		// to the mid-range value
		setTemp(float((getMaxTemp() + getMinTemp()) / 2));
		std::cout << "Temperature is now at " << getTemp() << "\n";
		std::cout << "Pressure is now at    " << getPres() << "\n\n";
	} else{
		// Reactor is in safe range
		std::cout << "Reactor is in safe range. No action being taken.\n\n";
	}
}
