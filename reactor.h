/**********************************************
* File: reactor.h
* Author: Alex Beck
* Email: abeck5@nd.edu
* 
* This is the header file for the reactor class 
**********************************************/

#ifndef REACTOR_H
#define REACTOR_H

#include <iostream>
#include <vector>
#include <fstream>

class Reactor{
	private:
		float temp, pres, maxTemp, minTemp, maxPres, minPres;
		int pumpspeed;
		
	public:
		// Default contructor calls invalid numbers
		Reactor() : temp((float)-1), pres((float)-1), maxTemp((float)-1), minTemp((float)-1),
			maxPres((float)-1), minPres((float)-1), pumpspeed(-1){} 

		Reactor(float tempIn, float presIn, float maxTempIn, float minTempIn, 
			float maxPresIn, float minPresIn, int speed) : 
			temp(tempIn), pres(presIn), maxTemp(maxTempIn), minTemp(minTempIn),
			maxPres(maxPresIn), minPres(minPresIn), pumpspeed(speed){}

		~Reactor() {} // Default destructor

		// Accessor methods

		float getTemp() const; // Gets temperature of coolant

		float getPres() const; // Gets pressure of coolant

		float getMaxTemp() const; // Gets max temperature 

		float getMinTemp() const; // Gets min temperature

		float getMaxPres() const; // Gets max pressure

		float getMinPres() const; // Gets min pressure
		
		int getPumpSpeed() const; // Gets current pump speed


		// Setter methods
		void setTemp(float newTemp); // Sets temperature variable

		void setPres(float newPres); // Sets Pressure temperature
		
		void setPumpSpeed(int inSpeed); // Sets pump speed



		void printData() const; // Prints the data for the object
		
		
		void checkTempAndPres(); // Checks to see if temp and pres are within range and changes variables as needed
		

};

#endif
